// Introduction

// What's an Object?
/* Think about objects in the non-programming sence, like a pen.
A pen can have different ink colours, be manufactured by different people,
have a different tip, and many other properties.

Similarly, an object in programming is an entity that has properties.
each property defines a characteristic of the object.
A property can be information associated with the object
(the colour of the pen) or action ( the pen's ability to write)
 */

// What does this have to do with code?

/* Object-oriented programming (OOP for short)
is a way to write programs using objects. When using OOP,
 you write, create, and modify objects, and the objects make
  up your program.

OOP changes the way a program is written and organized.
 So far, you've been writing function-based code, sometimes
  called the procedural programming.
Now let's discover how to write object-oriented code.
 */

// JavaScript and Objects

/*Like many other languages,
JavaScript involves programming objects,
so we can say it's an object-oriented language.
It provides a number of predefined objects while
also letting you create your own.
 */

// Creating an object

var pen = {
    type: 'ballpoint',
    colour: 'blue',
    brand: 'Bic'
};
// !! create a new object in JavaScript by setting its properties within a pair of curly braces: {...};

/*The above code defines a variable named pen whose value is an object:
you can therefore say pen is an object.
This object has three properties:
type,
color ,
and brand.
Each property has a name and a value and is separated
by a comma , (except the last one).
 */

// Getting a value
//After creating the object, you can access the value of its properties using dot notation such as myObject.myProperty .

console.log(pen.type); // will be 'ballpoint
console.log(pen.colour); // will be 'blue'
console.log(pen.brand); // wil be 'Bic'

console.log('My pen is a ' + pen.colour + ' ' + pen.brand + ' ' + pen.type + ' pen.');

// Modifying objects

//Once an object is created, you can change the values of its properties with the syntaxmyObject.myProperty = newValue.

pen.colour = 'red'; // modify the pen colour property

console.log('My pen is a ' + pen.colour + ' ' + pen.brand + ' ' + pen.type + ' pen.');
// Javascript even offers the ability to dynamically add new properties to an already created object.

var pen1 = {
    type: 'ballpoint',
    colour: 'blue',
    brand: 'Bic'
};

console.log('My pen is a ' + pen1.colour + ' ' + pen1.brand + ' ' + pen1.type + ' pen.');

pen1.price = '2.5'; // set the pen price property

console.log('My pen costs ' + pen1.price); // they get 'My pen costs $1" how???

// Example: Cake
/* Pens are fine,
but cake is cooler.
Let's create a cake in JavaScript that has several properties:
flavor, like vanilla, chocolate, etc.
price in dollars
layers (1, 2, 3...10?!)
 */

// Creating a cake.

var cake = {
    flavour: 'strawberry',
    layers: 2,
    price: 'R10',
    occasion: 'birthday',
};
// !! You can assign numbers, strings, and even other objects to properties!

console.log('The ' + cake.occasion + ' cake has a ' + cake.flavour + ' flavour, ' + cake.layers + ' layers, and costs ' + cake.price + '.');

// the cake is actually for a wedding!
cake.occasion = 'wedding';

// Its number of layers and price both go up.
cake.layers = cake.layers + 8;
cake.price = 'R50';

console.log('The ' + cake.occasion + ' cake has a ' + cake.flavour + ' flavour, ' + cake.layers + ' layers, and costs ' + cake.price + '.');

// Methods on objects
/*In the above code, we had to write lengthy console.log statements
each time to show the cake description.
There's a cleaner way to accomplish this.
 */

// Adding a method to an object
/* Observe the following example.
// Describe a cake
function describe(cake) {
    var description = "The " + cake.occasion + " cake has a " + cake.flavor + " flavor, " + cake.levels + " layers, and costs " + cake.price + ".";
    return description;
}

console.log(describe(cake));
 */

/* The function describe()  takes an object as a parameter.
We pass it the cake, and it accesses that object's properties
and prints them out in that sentence.

Now for an alternative approach:
creating a describe property that houses a method.
 */

var cake2 = {
    flavour: 'strawberry',
    layers: 2,
    price: 'R10',
    occasion: 'birthday',

    // Describe the cake
    describe: function () {
        var description = 'The ' + this.occasion + ' cake has a ' + this.flavour + ' flavour, ' + this.layers + ' layers, and costs ' + this.price + '.';
    return description
    }
};

console.log(cake2.describe());

// The cake is actually for a wedding!
cake2.occasion = 'wedding';

// Its number of layers and price both go up.
cake2.layers = cake2.layers + 8;
cake2.price = 'R50';

console.log(cake2.describe());

/* Now our object has a new property available to it, describe.
The value of this property is a function that returns a text
description of the cake.

A property whose value is a function is called a method.
 */

// !! Remember the parentheses, even if empty, when calling a method!

// The keyword "this"
/*Now look at the body of the  describe()  method on our cake object.
You see a new word: this.
This is automatically set by JavaScript inside a method and represents
the object on which the method was called.
 */