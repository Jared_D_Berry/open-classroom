// Conditions
// If Statement
//var number = Number (prompt('Please enter a number:'));
//if (number > 0) {
//    console.log (number + " is positive");
//}

//if (condition) {
    // Statements executed when the condition is true
//}
// the pair of opening and closing braces define what is called a BLOCK OF CODE associated with am IF statement.
// The condition is always placed in parentheses after the IF

// Conditions
/* is an expression that evaluates wheter something is true or falsse.
when the value of a condition is true, it is called this condition is satisfied
 */
// Booleans
/* this type has two possible values: TRUE and FALSE.
Any expression producing a Boolean value ( either true or fasle) can be condition in an IF statement
Boolean expressions can be created using the comparison operators
=== equal to
!== Not equal to
< less than
<= less than or equal to
> greater than
>= greater or equal to

its easy to confuse comparison operator like  ===  (or ==) with the assignment operator
(=). They're very, very different. Be warned!
 */

/*var number = Number(prompt('Please enter a number:'));
if (number >= 0) {
    console.log(number + ' is positive or zero');
}
// Alternative conditions
//Else lets enrich our sampple with different messages depending on whether the number is positive or not.
 */
var number = Number(prompt('Please enter a number:'));
if (number > 0) {
    console.log(number + ' is positive');
}
else {
    console.log(number + ' is negative or zero');
}
/* if (condition0 {
     code to run when condition is true
}
else {
    codeto run when condition is not true
}
*/
// Nesting condition
/*go next-level and display a specific message if the entered number is zero.
Use else if for a second conditional.
 */

var number = Number(prompt('Please enter a number:'));
if (number > 0) {
    console.log(number + ' is positive');
} else if (number < 0) {
    console.log(number + ' is negative');
}else {
    console.log(number + ' is zero');
}

// Add additional logic
// "AND" operator
/* Suppose you want to check if a number is between 0 and 100.
you're essentially checking if it's "greater than or equal to 0 " and " less than or equal to 100"
 */

if ((number >= 0) && (number <= 100)) {
    console.log(number + ' is between 0 and 100, both included');
}
// the && operator ("and") can apple to both types of boolean value.
//true will only be the result of the statement if both condition are true.
console.log(true && true); // true
console.log(true && false); // false
console.log(false && true); // false
console.log(false && false); // false

// "Or" operator
/* Now imagine you want to check that the number is outside the range of 0 and 100.
to meet this requirement, the number should be less than 0 or greater than 100.
 */
if ((number < 0) || (number > 100)) {
    console.log(number + ' is not in between 0 and 100');
}
// the || operator ("or") makes statements true if at least one of the statements is true.
console.log(true || true); // true
console.log(true || false); // true
console.log(false || true); // true
console.log(false || false); // false

//"Not" operator
// there's another operator for when you know what you don't want: the not operator! You'll use a ! for this.
if (!(number > 100)) {
    console.log(number + ' is less than or equal to 100');
}
// Therefore:
console.log(!true); // false
console.log(!false); // true
//Wild Right?!

// Multiple choices
// let's write some code that helps people decide what to wear based on the weather using if/else.
var weather = prompt("What's the weather like?");
if (weather === 'sunny') {
    console.log('T-shirt time');
} else if (weather === 'windy') {
    console.log('Windbreaker life.');
} else if (weather === 'rainy') {
    console.log('bring that umbrella!');
} else if (weather === 'snowy') {
    console.log('Just stay inside!');
} else {
    console.log('Not a valid weather type');
}
/* when a program should trigger a block from several operations depending on
the value of an expression,
you can write it using the JavaScript statement switch to do the same thing.
 */

var weather = prompt("Whats's the weather like?");
switch (weather) {
    case 'sunny':
        console.log('T-shirt time!');
        break;
    case 'Windy':
        console.log('Windbreaker life.');
        break;
    case 'rainy':
        console.log('Bring that umbrella');
        break;
    case 'snowy':
        console.log('Just stay inside!');
        break;
    default:
        console.log('Not a valid weather type');
}
/* the word switch kicks off the execution of one code block among many.
Only the code block that matches the relevant situation will be excuted.
 */
switch (expression) {
    case value1:
        // code when the expression matches value1
        break;
    case value2:
        // code when the expression matches value2
        break:
    ...
    default:
        // code for when neither case matches
}
/* you can set as many cases as you want!
the word default, which is put at the end of switch,
is optional.
it allows you to handle errors or unexpected values
 */
// break; in each block is important so you get out of the switch statement.
