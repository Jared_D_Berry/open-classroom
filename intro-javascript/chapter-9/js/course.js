// Introduction to Arrays
/*Imagine you want to create a list of all
the movies you've seen this year.

One solution would be to create film variables:
 */

var film1 = "Jurassic Park";
var film2 = "Titanic";
var film3 = "Toy Story";
// ...

/* If you're a movie buff,
you may quickly find yourself with too many
variables in your program. The worst part is that these variables
 are completely independent from one another.

Another possibility is to group the films in an object.
 */

var films = {
    film1: "Jurassic Park",
    film2: "Titanic",
    film3: "Toy Story",
    // ...
    };

/*This time, the data is centralized in the object films. However,
 the names of its properties (film1,film2,film3...) are unnecessary and
 repetitive.

We must find a solution to store items together without naming
them individually!

Luckily, there is one: use an array.
An array is a data type that can store a set of elements.
 */

// Manipulating arrays in JavaScript

/*In JavaScript, an array is an object that has special properties.

Create arrays

How to create our list of films in the form of a table.
 */

var films = ["Jurassic Park", "Titanic", "Toy Story"];

//Create a table with a pair of square brackets: []. Everything within the brackets makes up the array.

// !!You can store different types of elements within an array, including strings, numbers, and booleans:
var elements = ["Hello", 7, true];

//Since an array contains multiple things, it's good to name the array using a plural (for example,  films ).

//Identify an array's size

/*The number of elements stored in a table is called its size.
Here's how to access it.
 */

var films = ["Jurassic Park", "Titanic", "Toy Story"];

console.log(films.length); // will be 3

/*Add the above code to your chapter-9JavaScript file if you have one and open the HTML file in your browser to show the result.

You access the size of an array via its lengthproperty.
 */
// !!! Remember, we also used this length property to get the size of a string!
//Of course, this length property returns 0 for an empty array.

/*var emptyArray = []; // create an empty array

console.log(emptyArray).length); // will be 0
*/
// Access an element in an array

/*Each item in an array is identified by a number called its index.
 We can represent an array as a set of boxes, each storing a specific
 value and associated with an index.
Here is how one might represent the  films array:
 */
// Index Value =   0 = Jurassic Park
//                  1 = Titanic
//                  2 = Toy Story

//You can access a particular element by passing its index to the array name within square brackets:

var films = ["Jurassic Park", "Titanic", "Toy Story"];

console.log(films[0]); // will be "Jurassic Park"
console.log(films[1]); // will be "Titanic"
console.log(films[2]); // will be "Toy Story"

/* That's exactly the way you accessed characters in a string! The same golden rules apply:

The index of the first element of an array is 0, not 1.

The highest index number is the array's length minus 1.

Using an invalid index to access a JavaScript array element returns the value undefined.
 */

// ...

console.log(films[3]); // will be undefined

// Browse an array

/*There are two ways to browse an array element by element.

The first is to use a for  loop as discussed previously in the course.
 */

var films = ["Jurassic Park", "Titanic", "Toy Story"];

for (var i = 0; i < films.length; i++) {
    console.log(films[i]);
}

/*The for loop runs through each element in the array starting with
index 0 all the way up to the length of the array minus 1,
 which will be its last value.
 */

//Add an element to an array

/*Let's say you just watched The Revenant and you want to add it to
 the list. Here's how you'd do so:
 */

var films = ["Jurassic Park", "Titanic", "Toy Story"];

films.push("The Revenant");

console.log(films[3]); // will show "The Revenant"

/*You add a new item to an array with the method push().
You pass the new element to be added as a parameter to the method.
 */

// Object arrays

/*An array can store any type of item, including objects and even
other arrays!

Say you want to also store the year of release of each film seen
this year.
One solution is to store these dates directly in the array as their
own items.
 */

var films = ["Jurassic Park", 1993, "Titanic", 1997, "Toy Story", 1995];

/*However, this makes things tricky since now, when accessing the
element at a particular index, you can't be sure which year corresponds
 to which film. Imagine now adding directors and other elements.
 Too tough!

A better approach would be to represent each film as an object.
 */
var Film = {
    // Initialise le film
    init: function (title, year) {
        this.title = title;
        this.year = year;
    },
    // Renvoie la description du film
    describe: function () {
        var description = this.title + " (" + this.year + ")";
        return description;
    }
};

var film1 = Object.create(Film);
film1.init("Jurassic Park", 1993);

var film2 = Object.create(Film);
film2.init("Titanic", 1997);

var film3 = Object.create(Film);
film3.init("Toy Story", 1995);

/*The Film object is a model for each item. init()  gives each film
a title and a year, and the  describe() method writes out each film
as "Title (year)".

Objects film1,film2 and film3 are created with Film as a prototype,
and they therefore have its properties.

Now you can create an array called films  containing our
objects and then use it to display the description of each film.
 */

// ...

var films = [];
films.push(film1);
films.push(film2);
films.push(film3);

films.forEach(function (film) {
    console.log(film.describe());
});

/*In our example, the function associated with the method forEach()
displays the result of the method describe() on each object in the
array of films.
 */