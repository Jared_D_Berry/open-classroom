//Variables
var a;
console.log(a);

var b;
b = 3.14;
console.log(b);

var c = 4.13;
console.log(c);

//Increment operator
var d = 0; //d contains 0
d += 1; // d contains 1
d++; // d contains 2
console.log(d); // show 2

//Type conversion
var f = 100;
console.log('the variable f contains the value: ' + f);

// NaN ( Not a Number
var g = 'five' * 2;
console.log(g); // Shows NaN

// Explicit conversion (has Numbers and String Commands)
var h = '5';
console.log(h+1); // concatenation: shows the string "51"
h = Number('5');
console.log(h+1); // Addition: shows the number 6

// Naming variables
// Naming variables clearly can make your code much easier to read.
//Two examples
var nb1 = 5.5;
var nb2 = 3.14;
var nb3 = 2 * nb2 * nb1;
console.log(nb3);

var radius = 5.5;
var pi = 3.14;
var circumference = 2 * pi * radius;
console.log(circumference);
/* Choosing maeaningful names
        most important rule is to give each vaiable a name that reflects its role.
   Don't use reserved words
   Follow conventions
        camelCase is the most commom
                All variables names begin with lowercase letter
                variable consists of several words, the first letter of each word ( execept the first word) is uppercase
 */
