// Beyond cake
var cake = {
    flavor: "strawberry",
    levels: 2,
    price: "$10",
    occasion: "birthday",

    // Describe the cake
    describe: function () {
        var description = "The " + this.occasion + " cake has a " + this.flavor + " flavor, " + this.levels + " layers, and costs " + this.price + ".";
        return description;
    }
};

console.log(cake.describe());

// The cake is actually for a wedding!
cake.occasion = "wedding";

// Its number of layers and price both go up.
cake.levels = cake.levels + 8;
cake.price = "$50";

console.log(cake.describe());

var muffin = {
    type: "muffin",
    flavor: "blueberry",
    levels: 1,
    price: "$2",
    occasion: "breakfast",

    // Describe the pastry
    describe: function () {
        var description = "The " + this.type + " is a " + this.occasion + " pastry, has a " + this.flavor + " flavor, " + this.levels + " layers, and costs " + this.price + ".";
        return description;
    }
};

console.log(muffin.describe());

// You'll notice that the cake code and muffin code are very similar. It'd make more sense for us to create a sort of pastry factory or bakery to create code for individual pastries, no?

// Objects and prototypes in JavaScript
/*In addition to its special properties,
every JavaScript object has an internal property called prototype .
This is a link (known as reference ) to another object.
When trying to access a property that does not exist in an object,
JavaScript tries to find this property in the prototype of this object.
 */

// Example
var anObject = {
    a: 2
};

// Create anotherObject using anObject as a prototype
var anotherObject = Object.create(anObject);

console.log(anotherObject.a); // will show 2

/* In this example, the JavaScript statement Object.create() is used
to create the objectanotherObject  based on the prototype object
anObject.

If the prototype of an object does not have a desired property,
 then research continues in its own own prototype until we get to
 the end of prototype chain.
If the property was found in objects, access returns the value undefined
 */

console.log(anotherObject.b); // will be underfined

/* This type of relationship between JavaScrip objects is called delegation:
an object delegates part of its operation to its prototpye.
 */

// Cake prototype

var Pastry = {
    type: '',
    flavour: '',
    levels: 0,
    price: '',
    occasion: '',

    // Describe the pastry
    describe: function () {
        var description = 'The ' + this.type + ' is a ' + this.occasion + ' pastry, has a ' + this.flavour + ' flavour, ' + this.levels + ' layer(s), and costa ' + this.price + '.';
    return description;
    }
};

var muffin = Object.create(Pastry);
muffin.type = 'muffin';
muffin.flavour = 'blueberry';
muffin.levels = 1;
muffin.price = 'R2';
muffin.occasion = 'breakfast';

var cake = Object.create(Pastry);
cake.type = 'cake';
cake.flavour = 'vanilla';
cake.levels = 3;
cake.price = 'R10';
cake.occasion = 'birthday';

console.log(muffin.describe());
console.log(cake.describe());

/*In this example, we created an object named Pastry ,
 which brings together the properties common to all the characters.
  The cake  and muffin  are created via  Pastry  as a prototype,
  which delegates its features to them.

The base object (here,  Pastry ) often begins with a capital letter,
but this is not an obligation.
 */

// Initializing objects
/* The process of creating a Pastry is a little repetitive:
for each character, you must successively give a value to each
of its properties.
You can do better by creating an initialization function.
 */

var Pastry = {
    // initialize the pastry
    init: function (type, flavour, levels, price, occasion) {
        this.type = type;
        this.flavour = flavour;
        this.levels = levels;
        this.price = price;
        this.occasion = occasion;
    },

    // Describe the pastry
    describe: function () {
        var description = 'The ' + this.type + ' is a ' + this.occasion +' pastry, has a ' + this.flavour + ' flavour, ' + this.levels + ' layer(s), and costs ' + this.price + '.';
        return description;
    }
};

var muffin = Object.create(Pastry);
muffin.init('muffin', 'blueberry', 1, 'R10', 'breakfast');

var cake = Object.create(Pastry);
cake.init('cake', 'vanilla', 3, 'R10', 'birthday');

console.log(muffin.describe());
console.log(cake.describe());

/*The method init() takes the initial property values of the pastry
 as parameters. The pastry creation code is therefore reduced to only
  2 steps:

The actual creation, with the  Pastry  object as a prototype,

The initialization of its properties, using the function init()
on the Pastry Object.
 */

var Pastry = {
    //initiallize the pastry
    init: function (type, flavour, levels, price, occasion) {
        this.type = type;
        this.flavour = flavour;
        this.levels = levels;
        this.price = price;
        this.occasion = occasion;
    },

    // Describe the pastry
    describe: function () {
        var description = 'The ' + this.type + ' is a ' + this.occasion + ' pastry, has a ' + this.flavour + ' flavour, ' +this.levels + ' layer(s), and costs ' + this.price + '.';
        return description;
    },

    bake: function () {
        var baked = 'The ' + this.flavour + ' ' + this.type + " was placed in the oven. It's done!"
        return baked
    },

    //!!Take a moment and write a method to eat the pastry using the same strategy!!
    eat: function () {
        var eat = 'I ate the ' + this.flavour + ' ' + this.type + ' for the ' + this.occasion + ' but it was not worth ' + this.price + '.'
        return eat
    }
};

var muffin = Object.create(Pastry);
muffin.init('muffin', 'blueberry', 1, 'R2', 'breakfast');

var cake = Object.create(Pastry);
cake.init('cake', 'vanilla', 3, 'R10', 'birthday');

console.log(muffin.bake());
console.log(cake.bake());
console.log(muffin.describe());
console.log(cake.describe());
console.log(muffin.eat());
console.log(cake.eat());
//Take a moment and write a method to eat the pastry using the same strategy!!


// Next Level: Make A Game

/* The baking example was relatively simple,
but you can get as complicated as you like with this structure
(defining objects, creating objects, doing things with them through
a series of methods).
 */

/*Let's go next-level now and look at an example with more significant
interactions between objects.
In this game, characters, players, and enemies are created.
A player can attack an opponent, but the reverse is also true!
If attacked, a character sees their life points decrease from the
force of the attack.
If a character's number of points reaches 0,
then the character is eliminated.
If the winner is a player,
they receive a number of experience points equal to the value of
the eliminated opponent.
 */

var Character = {
    //Initializa the character
    initCharacter: function (name, health, force) {
        this.name = name;
        this.health = health;
        this.force = force;
    },
    // Attack a target
    attack: function (target) {
        if (this.health > 0) {
            var damage = this.force;
            console.log(this.name + ' attacks ' + target.name + ' and causes ' + damage + ' damage points');
            target.health = target.health - damage;
            if (target.health > 0) {
                console.log(target.name + ' has ' + target.health + ' health points left');
            } else {
                target.health = 0;
                console.log(target.name + ' has been eliminated!');
            }
        } else {
            console.log(this.name + " can't attack (they've been eliminated).");
        }
    }
};

var Player = Object.create(Character);
// Initialize the player
Player.initPlayer = function (name, health, force) {
    this.initCharacter(name, health, force);
    this.xp = 0;
};
// Describe the player
Player.describe = function () {
    var description = this.name + ' has ' + this.health + ' health points, ' +
        this.force + ' force point ' + this.xp + ' experience points';
    return description;
};
// Fight an enemy
Player.fight = function (enemy) {
    this.attack(enemy);
    if (enemy.health ===0) {
        console.log(this.name + ' eliminate ' + enemy.name + ' and wins ' +
            enemy.value + ' experience points');
        this.xp += enemy.value;
    }
};

var Enemy = Object.create(Character);
// Initialize the enemy
Enemy.initEnemy = function (name, health, force, species, value) {
    this.initCharacter(name, health, force);
    this.species = species;
    this.value = value;
};

// ...
// Once modeled, the objects can then interact with one another through method calls
// ...
var player1 = Object.create(Player);
player1.initPlayer('Rainbow Dash', 150, 25);

var player2 = Object.create(Player);
player2.initPlayer('Applejack', 130, 30);

console.log('Welcome to the adventure! Here are are heros:');
console.log(player1.describe());
console.log(player2.describe());

var monster = Object.create(Enemy);
monster.initEnemy('Spike', 40,20, 'orc', 10);

console.log("A wild monster has appeared: it's a(n) " + monster.species + " name " +
monster.name);

monster.attack(player1);
monster.attack(player2);

player1.fight(monster);
player2.fight(monster);

console.log(player1.describe());
console.log(player2.describe());

/*Test with other initial values for health and force  properties,
and you'll get different outcomes
 */

// Chapter 12 - Use Constructor Functions

/*In the last chapter we talked about object prototypes
and how to create new objects using theObject.create function.
Note that after we created our new objects, we invoked a function
we had created called init. There's often code like this init
function that you want to run in order to "set up" your new object
correctly, and most object-oriented languages even have some concept
of this init function built directly into them. In Javascript,
that built-in init function is called a constructor function,
and we can invoke it by using the special Javascript keyword new.
 */
/*To create an object using constructor functions,
we simply write a standalone function containing the init
functionality that we want for our object. Instead of calling
it init, this function will have the name of our object's "class."
Here's how it would look if we rewrote some of our pastry code from
last chapter, using a constructor function:
 */

var Pastry = {
    // initialize the pastry
    init: function (type, flavor, levels, price, occasion) {
        this.type = type;
        this.flavor = flavor;
        this.levels = levels;
        this.price = price;
        this.occasion = occasion;
    },
    ...
}

// will become

function Pastry(type, flavor, levels, price, occasion) {
    this.type = type;
    this.flavor = flavor;
    this.levels = levels;
    this.price = price;
    this.occasion = occasion;
}

//Now, to instantiate it, instead of calling

Object.create(Pastry);

// and then an init function, we'll simply do

new Pastry(type, flavor, levels, price, occasion);

/*What about the other function that was defined
by our Pastry object, though? Remember that our pastries
had a "describe" function:
 */

var Pastry = {
    // initialize the pastry
    init: function (type, flavor, levels, price, occasion) {
        this.type = type;
        this.flavor = flavor;
        this.levels = levels;
        this.price = price;
        this.occasion = occasion;
    },

    // Describe the pastry
    describe: function () {
        var description = "The " + this.type + " is a " + this.occasion + " pastry, has a " + this.flavor + " flavor, " + this.levels + " layer(s), and costs " + this.price + ".";
        return description;
    }
};

// This is where the Prototype comes in again.

/* Remember that the Pastry object last chapter
served as the prototype for all the pastries we've
created. All the functions, like init and describe,
that we had defined on the Pastry object, were automatically
available on all the pastries because we passed Pastry in to
Object.create when we created them.
 */

/* Using the constructor function is slightly different, though.
We can't just add properties to our Pastry constructor function,
and expect them to be present on the instantiated instances of
our class, because constructors aren't the same thing as the
prototype of the objects they create.
 */

/* Fortunately, constructor functions do have a special reference
to the prototype of the objects they create, though, and you can
attach inheritable properties to it, just as we did to the prototype
in the last chapter. Now that we're using a constructor function,
we'll assign our describe function to the Pastry prototype like
this:
 */

function Pastry(type, flavor, levels, price, occasion) {
    this.type = type;
    this.flavor = flavor;
    this.levels = levels;
    this.price = price;
    this.occasion = occasion;
}

Pastry.prototype.describe = function () {
    var description = "The " + this.type + " is a " + this.occasion + "pastry, has a " + this.flavor + " flavor, " + this.levels + " layer(s), and costs " + this.price + ".";
    return description;
}

// In the last chapter we had two lines to create every new object:

var muffin = Object.create(Pastry);
muffin.init("muffin", "blueberry", 1, "$2", "breakfast");

var cake = Object.create(Pastry);
cake.init("cake", "vanilla", 3, "$10", "birthday");

console.log(muffin.describe());
console.log(cake.describe());

/*Now, using constructor functions,
we can instantiate our objects on just one line
and have the same result:
 */

var muffin = new Pastry("muffin", "blueberry", 1, "$2", "breakfast");
var cake = new Pastry("cake", "vanilla", 3, "$10", "birthday");

console.log(muffin.describe());
console.log(cake.describe());

// !!! Don't forget "new" when calling constructor functions!

// There's an important gotcha with constructor functions.

/*It's possible to forget to use the new keyword and instead
call the Pastry function as a normal function, expecting it
to return a pastry object. Notice though, that our constructor
function doesn't have a return statement at all. The return value
 of a function that doesn't specify any return value, is
  "undefined," so that's what you get when you call your constructor
  function without using the specialnew keyword. This can lead to
  bugs when you or another user of your object library makes
  this fairly common oversight:
 */

// gonna instantiate some sweet beignets...
var beignet = Pastry("beignet", "cream", 1, "$1.50", "anytime you want beignets");

console.log(beignet.describe());
> Uncaught TypeError: Cannot read property 'describe' of undefined

//This would almost certainly cause your function to fail, if you're lucky!

/*A possibly worse, and harder to debug, danger here is
not what happens when you callbeignet.describe(),
but something that happened silently when you called
your constructor function without using the new keyword.
 */

//Check the value of window.type after you run the Pastry function without the new keyword:

var beignet = Pastry("beignet", "cream", 1, "$1.50", "anytime you want beignets");
window.type
> beignet

/*You may not be familiar with the window object
(we'll get into that more in the next chapter),
but it's the root and basis for your entire webpage,
and unintentionally assigning properties to it is really
dangerous for your program, especially if you overwrite
existing properties by accident.

We'll talk more about how this happens when we take a
 look at the ideas of scope and the global object in
 the next chapter. For now, remember that there's a danger
 in omitting the new keyword when you're trying to
initialize an object with a constructor function.
 */

// Chapter 13 - Understand Scope
// How scope work in JavaScript

/* You can't understand JavaScript without understanding how scopes work.

Scope can seem like a bit of an abstract concept,
but it's faily simple in practice.
A scope is basically the area in which the name of a variable or
function has a given meaning.

You know that in JavaScript you can assign a value to a variable name
and then reference that name to get that value back:
 */

var foo = 'bar';
console.log(foo) // --> bar

/* That's because you reference the variable in the same scope where
you defined it. Variables and functions, then, are available for use
in the scopes where they were defined.

There are two kinds of scopes in JavaScript:
1) the global scope
2) local scope
, AKA the scope of each function in your program.

You can think of the global scope as just a giant scope that contains
all the other scopes. When you aren't inside any function, you're in
the global scope. In the code example above, the variable "foo" is
defined on the global scope.

Now, since each function has its own scope, this means that a variable
defined inside a function can only be read inside that function.
 */

// Example:

function myFunction() {
    var foo = 'bar';
}
console.log(foo);
// > Uncaught ReferenceError: foo is not defined

/* The name foo in this case is created inside myFunction,
so the rest of the program can't access it.
It's private to that function.
 */

// Nesting.
/* Scopes are nested in JavaScript.
this means that inside a function, you can call ( and write to)
all the variables and functions from the global scopes, as well
as those from any other function that your functions happens to
be nested *inside* of.
 */

// An analogy: Nested scopes are like the nested regional laws within a federalized country

/* If this all seems a bit abstract, it may be helpful to think
of scopes in Javascript as analogous to the different national and
regional laws within a federalized country.

Take the United States as an example:
everyone in every state and county is subject to the federal
laws of the nation as a whole. In this way, federal law is like
the global scope.

When we look at the state level on the other hand,
we see that the laws of each state only apply within that state.
New Jersey has different laws about labor, taxation, and pumping gas,
than Pennsylvania does. When we consider the laws of counties and
cities, the "scope" of their laws becomes even more specific.

In Javascript, scopes work in a somewhat similar way,
with functions replacing the states and counties, and
variable and function names replacing the laws in the analogy.
Let's look at an example to see how this works:
 */

var equalProtection = '... nor shall any State deprive any person of life, liberty, or property, without due process of law; nor deny to any person within its jurisdiction the equal protection of the laws ';

function newJersey() {
    console.log(noSelfServeGas); //--> Sorry, you can't pump your own gas.
    console.log(equalProtection); // --> nor shall any State etc.
    }
}

function Pennsylvania() {
    console.log(noSelfServeGas); //--> Uncaught ReferenceError: noSelfServeGas is not defined
    console.log(equalProtection): //--> nor shall any State etc.

}

/* Here, in addition to the global scope, which represents
national law, I've defined scopes for the states of New Jersey
and Pennsylvania, and for two counties in New Jersey.
I've also assigned two variables:

equalProtection is on the global scope and represents the equal
protection clause of the 14th amendment to the US constitution,
which applies as a law everywhere in the country.

noSelfServeGas  is on the scope of the newJersey function and
represents New Jersey's law forbidding self-service gas stations,
and like all state laws, only applies in the state where it was
defined.

The point of this is to show you that its pretty intuitive to
understand where variable and function names apply in your code.
We see how, beyond its own scope, each function can also access
 everything in the scope where it was defined. This makes sense
 because, since your function is defined inside another function,
 all of the code in its body is also inside that other "parent"
 function. Thus, code can refer to
anything that was defined in any of its "ancestor" scopes:
 */

var foo = "bar";

function one() {
    var foo1 = "bar1";
    // you can reference 'foo' and 'foo1' here.

    function two() {
        var foo2 = "bar2";
        // you can reference 'foo', 'foo1', and 'foo2' here.

        function three() {
            var foo3 = "bar3";
            // you can reference 'foo', 'foo1', 'foo2', and 'foo3' here.
        }
    }
}

/* However, what happens when a function defines a variable
local to itself, that has the same name as a variable that
was defined on one of its parent scopes?
 */

//!!This is where this analogy breaks down.

/*In laws, the nation's laws usually take precedence over the local
laws of zones within the nation. In the United States, federal laws
take precedence over state laws, state laws take precedence over
city laws, and so on.

In Javascript, though, it's the reverse. Local names take
precedence over names defined on parent scopes. For example,
if the world were Javascript, a town in New Jersey could pass
a law eliminating the equal protection clause of the 14th
amendment of the US constitution within the town itself.
New Jersey still wouldn't be able to modify the laws of a
neighboring state, but it could overwrite or modify the laws of
the "global scope", i.e. the whole country.

Fortunately, that's not the case in the real world.

Take the previous code example again: if we rewrote it
to reuse the "foo" variable name, the value of "foo" would change
depending on which scope you were accessing it from:
 */

var foo = "bar";

function one() {
    var foo = "bar1";
    // this is in the scope of the function "one", foo here will be "bar1"

    function two() {
        var foo = "bar2";
        // this is in the scope of the function "two", foo here will be "bar2"

        function three() {
            var foo = "bar3";
            // this is in the scope of the function "three", foo here will be "bar3"
        }
    }
}

// this is in the global scope, foo here will be "bar"

// The Global Object

/*Earlier, we said that the global scope was like a scope for all
the code that exists outside of any function.
It's as if Javascript were "making up" a scope for all the code
that is outside of any function, and pretending that all that code
was wrapped in one big function body.

The global scope in Javascript is special in another way as well,
because it behaves like an object.

This object usually has a name, which varies depends on the context
 in which your javascript code is running.
In the browser it is usually called "window".
 */

/* !!! In other Javascript environments,
the global object has different names.
In Node.js, the global object is usually called "global,"
and web workers refer to it as "self."
 */

/*Unlike functions, defining a variable on the global
scope causes it to be available as a property of the
global object:
 */

var foo = "bar";

function baz() {
    console.log(foo);
    console.log(this.foo);
}

baz();
// > "bar"
// > "bar"

/* By contrast, defining a variable inside a function,
does not mean that that variable will be available
on that function's object:
 */

function baz() {
    var foo = "bar";

    console.log(foo);
    console.log(this.foo);
}

baz();
// > "bar"
// > undefined

/* Furthermore, just as javascript assumes the global scope for
code that isn't in another function, it assumes that this refers
to the global object unless the function is bound to another object.
That's why we can call this.foo on line 5 in the earlier example,
and it's also the source of the bug that we encountered at the end
of last chapter, when we tried to instantiate our pastry object
without using thenew keyword, and instead ended up writing our
pastry's attributes onto the global object.

This has some big implications for bugs in object-oriented programming.

Unfortunately, it means that if you write some code using the keyword
this, and someone runs that code in a context other than the one it
was meant for, this could suddenly mean the global window object
instead of the object that the function's author intended.

This is exactly what happens when someone calls
(from the global scope itself) a constructor function without
using the new keyword. Remember that constructor functions
called with thenew keyword create a new object, and this inside
the constructor function will refer to that object.
Constructor functions called without the new keyword,
on the other hand, are just normal functions,
so callingthis inside them will refer to whatever the meaning of
this was in the scope in which the constructor function was defined
(usually the global object).

In our Pastry example from last chapter,
when we forgot to use the new keyword, all of the properties we
wanted to assign to our new object got assigned to the global
scope instead, and we ended up with this side-effect:
 */

window.type
// > "beignet"

/*To see what this can lead to, try going to a web page
(one that you don't mind unexpectedly navigating away from),
and typing the following code
into the developer tools console:
 */

function Ache(intensity, location) {
    this.intensity = intensity;
    this.location = location;
}
var headache = Ache(1, 'head');

/* The problem is that the code is expecting to set
ache.location but is in fact settingwindow.location ,
a special property
that controls the browser's url address!
 */

//instanceof (and protecting constructor functions against use without new)

/*There's a handy operator in Javascript called instanceof.
It can be used to tell you if an object was instantiated from
 a given constructor function:
 */

function MyConstructor() {};

var construction = new MyConstructor();
construction instanceof MyConstructor;
// > true

var whoopsForgotToUseNew = MyConstructor();
whoopsForgotToUseNew instanceof MyConstructor;
// > false

/* Some programmers like to rescue code that forgets to use the new
keyword. They do this by adding a check inside their constructor
functions to see if the function is being called without the
 new keyword. This isn't required, but it's a useful technique to
 know and can help prevent bugs down the road when your object
 library is being used in unexpected ways.

If we rewrite our Pastry function to do this, it will use instanceof
to make sure that this

is actually a Pastry object, and not, for example, the global window.
If it isn't, we can assume that the new keyword has been
unintentionally omitted, and we can even help the user in that case by
calling the constructor function again (the right way this time) and
returning that value from thePastry function:
 */

function Pastry(type, flavor, levels, price, occasion) {
    if !( this instanceof Pastry ) {
        return new Pastry(type, flavor, levels, price, occasion);
    }

    // the rest of the function goes here...
}
